module.exports = function (app, addon, io) {

    // Root route. This route will serve the `atlassian-connect.json` unless the
    // documentation url inside `atlassian-connect.json` is set
    app.get('/', function (req, res) {
        res.format({
            // If the request content-type is text-html, it will decide which to serve up
            'text/html': function () {
                res.redirect('/atlassian-connect.json');
            },
            // This logic is here to make sure that the `atlassian-connect.json` is always
            // served up when requested by the host
            'application/json': function () {
                res.redirect('/atlassian-connect.json');
            }
        });
    });

    var whoslooking = {};
    var rooms = {};

    io.on('connection', function(socket) {
      socket.on('message', function (message) {
        var data = JSON.parse(message);
        if (!data.clientKey || !data.issueKey || !data.user) {
            console.warn("Invalid message!", data);
            return;
        }
        var room = data.clientKey + '/' + data.issueKey;
        if (this.rooms().length == 0) {
          this.join(room);
          rooms[this.id] = room;
        }
        var users = whoslooking[room];
        if (!users) {
          users = whoslooking[room] = {};
        }
        users[this.id] = data.user;
        message = JSON.stringify(users);
        // except this socket
        this.room(room).send(message);
        // this socket
        this.send(message);
      });
      socket.on('close', function() {
        var room = rooms[this.id];
        if (!room) {
            console.log('No room');
            return;
        }
        delete rooms[this.id];
        var users = whoslooking[room];
        delete users[this.id];
        this.room(room).send(JSON.stringify(users));
      });
    });

    app.get('/whoslooking', addon.authenticate(), function (req, res) {

        var clientKey = req.session.clientKey || 'test';
        var issueKey = req.query.issue_key || 'DEMO-1';
        var nsp = '/' + clientKey + '/' + issueKey;

        res.render('whoslooking', {
            clientKey: clientKey,
            issueKey: issueKey
        });
    });

};
