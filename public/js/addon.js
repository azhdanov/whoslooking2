$(function() {
  var getMeta = function(name) {
    return $('meta[name='+name+']').attr("content")
  };
  var avatarSize = 16;
  var hostBaseUrl = getMeta('host-base-url');
  var atlToken = getMeta('atl-token');
  var clientKey = getMeta('client-key');
  var issueKey = getMeta('issue-key');
  AP.getUser(function(remoteUser) {
    var socket = new eio.Socket('/?jwt=' + atlToken);
    socket.on('message', function(message) {
      var whoslooking = JSON.parse(message);
      var viewerList = $('<p>').addClass('whoslooking-onlookers-list').attr('id', 'whoslooking-onlookers-list');
      for (var id in whoslooking) {
        var user = JSON.parse(whoslooking[id]);
        if (user.key != remoteUser.key) {
          var avatarUrl = hostBaseUrl + "/secure/useravatar?ownerId=" + user.id;
          var viewerListItem = $('<p>');
          user.displayName = user.fullName ? user.fullName : user.key;
          var userDisplayName = $('<span>').text(user.displayName).addClass('whoslooking-displayname');
          var avatar = $('<img>').attr('width', avatarSize + 'px').attr('height', avatarSize + 'px').attr('src', avatarUrl).addClass('whoslooking-avatar');
          viewerListItem.append(avatar).append(userDisplayName);
          viewerList.append(viewerListItem);
        }
      }
      $('#whoslooking-onlookers-list').replaceWith(viewerList);
      AP.resize();     
    });
    var data = {
      clientKey: clientKey,
      issueKey: issueKey,
      user: JSON.stringify(remoteUser)
    }
    socket.on('open', function() {
      socket.send(JSON.stringify(data));
    });
    socket.on('close', function() {
      setTimeout(function() {
        socket.open();
      }, 1000);
    });
  });
});